//
//  Font.swift
//  Lets Connect
//
//  Created by Lets Connect on 09/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

extension Font{
    public static func robotoRegular(size: CGFloat) -> Font {
        return Font.custom("Roboto-Regular", size: size)
    }
    public static func robotoMedium(size: CGFloat) -> Font {
        return Font.custom("Roboto-Medium", size: size)
    }
    public static func robotoLight(size: CGFloat) -> Font {
        return Font.custom("Roboto-Light", size: size)
    }
    public static func robotoThin(size: CGFloat) -> Font {
        return Font.custom("Roboto-Thin", size: size)
    }
    public static func robotoBold(size: CGFloat) -> Font {
        return Font.custom("Roboto-Bold", size: size)
    }
    public static func robotoBlack(size: CGFloat) -> Font {
        return Font.custom("Roboto-Black", size: size)
    }
}
