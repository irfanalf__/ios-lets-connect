//
//  DataResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import Alamofire

extension DataResponse where Failure == AFError, Success: BaseResponse {
    func consumeResult(onSuccess: (Success) -> Void, onError: (NetworkException) -> Void){
        switch(self.result){
        case .success(let data):
            if(self.response?.statusCode == 200){
                if(data.status){
                    onSuccess(data)
                }else {
                    onError(ResponseError())
                }
            }else {
                onError(HttpError())
            }
            break
        case .failure(let error):
            switch(error){
            case .invalidURL(_):
                onError(InvalidURLError())
                break
            case .createUploadableFailed(_):
                onError(CreateUploadableError())
                break
            case .createURLRequestFailed(_):
                onError(CreateUrlRequestError())
                break
            case .downloadedFileMoveFailed(_):
                onError(DownloadedFileMoveError())
                break
            case .explicitlyCancelled:
                onError(ExplicitlyCancelledError())
                break
            case .multipartEncodingFailed(_):
                onError(MultipartEncodingError())
                break
            case .parameterEncodingFailed(_):
                onError(ParameterEncodingError())
                break
            case .parameterEncoderFailed(_):
                onError(ParameterEncodingError())
                break
            case .requestAdaptationFailed(_):
                onError(RequestAdaptationError())
                break
            case .requestRetryFailed(_):
                onError(RequestRetryError())
                break
            case .responseValidationFailed(_):
                onError(ResponseValidationError())
                break
            case .responseSerializationFailed(_):
                onError(ResponseSerializationError())
                break
            case .serverTrustEvaluationFailed(_):
                onError(ServerTrustEvaluationError())
                break
            case .sessionDeinitialized:
                onError(SessionDeinitializedError())
                break
            case .sessionInvalidated(_):
                onError(SessionInvalidateError())
                break
            case .sessionTaskFailed(_):
                onError(SessionTaskError())
                break
            case .urlRequestValidationFailed(_):
                onError(RequestValidationError())
                break
            }
            break
        }
    }
}

