//
//  Color.swift
//  Lets Connect
//
//  Created by Lets Connect on 09/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

extension Color{
    init(hex: Int, alpha: Double = 1) {
        let components = (
            R: Double((hex >> 16) & 0xff) / 255,
            G: Double((hex >> 08) & 0xff) / 255,
            B: Double((hex >> 00) & 0xff) / 255
        )
        self.init(
            .sRGB,
            red: components.R,
            green: components.G,
            blue: components.B,
            opacity: alpha
        )
    }
    
    public static let red5757 = Color(hex: 0xFF5757)
    public static let blueBCFF = Color(hex: 0x42BCFF)
}
