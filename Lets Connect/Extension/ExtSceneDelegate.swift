//
//  SceneDelegate.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

extension SceneDelegate {
    enum Route {
        //Onboarding relates routes
        case Onboarding
        case OnboardingDiscover
        
        //Login relates routes
        case Login
    }
}
