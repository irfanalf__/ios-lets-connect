//
//  SendMessageAck.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SendMessageAck: BaseResponse, Codable{
    var status: Bool
    var message: String
    let conversationId: String
    let receivedByServerAt: UInt64
    
    private enum CodingKeys: String, CodingKey {
        case status
        case message
        case conversationId = "conversation_id"
        case receivedByServerAt = "received_by_server_at"
    }
}
