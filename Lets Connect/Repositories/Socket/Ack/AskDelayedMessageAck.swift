//
//  AskDelayedMessageAck.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct AskDelayedMessageAck: BaseResponse, Codable {
    var status: Bool
    var message: String
    let data: [AskDelayedMessageData]
    
    private enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
    
    struct AskDelayedMessageData: Codable {
        let conversationId: String
        let conversation: String
        let replyTo: String?
        let receiverId: String
        let receivedByTargetAt: Double
        let sender: Sender
        
        private enum CodingKeys: String, CodingKey {
            case conversationId = "conversation_id"
            case conversation
            case replyTo = "reply_to"
            case receiverId = "receiver_id"
            case receivedByTargetAt = "received_by_target_at"
            case sender
        }
        
        struct Sender: Codable{
            let id: Int
            let email: String
            let fullname: String
            let image: String
            let username: String
            
            private enum CodingKeys: String, CodingKey {
                case id
                case email
                case fullname
                case image
                case username
            }
        }
    }
}
