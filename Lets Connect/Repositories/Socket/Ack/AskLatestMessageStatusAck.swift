//
//  AskLatestMessageStatusAck.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct AskLatestMessageStatusAck: BaseResponse, Codable{
    var status: Bool
    var message: String
    let data: [Message]
    
    private enum CodingKeys: String, CodingKey{
        case status
        case message
        case data
    }
    
    struct Message: Codable{
        let conversationId: String
        let status: Int
        let sentAt: Double
        let readAt: Double
        
        private enum CodingKeys: String, CodingKey {
            case conversationId = "conversation_id"
            case status
            case sentAt = "sent_at"
            case readAt = "read_at"
        }
    }
}
