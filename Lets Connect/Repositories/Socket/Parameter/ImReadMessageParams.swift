//
//  ImReadMessageParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct ImReadMessageParams: Codable {
    let senderId: Int
    
    private enum CodingKeys: String, CodingKey {
        case senderId = "sender_id"
    }
}
