//
//  SendMessageParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation


struct SendMessageParams: Codable {
    let conversationId: String?
    let targetId: Int?
    let conversation: String?
    let replyTo: String?
    let sendAt: Double
    
    private enum CodingKeys: String, CodingKey {
        case conversationId = "conversation_id"
        case targetId = "target_id"
        case conversation
        case replyTo = "reply_to"
        case sendAt = "send_at"
    }
}
