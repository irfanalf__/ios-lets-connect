//
//  TypingParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct TypingParams: Codable {
    let targetId: Int
    
    private enum CodingKeys: String, CodingKey {
        case targetId = "target_id"
    }
}
