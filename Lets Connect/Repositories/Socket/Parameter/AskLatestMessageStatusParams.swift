//
//  AskLatestMessageStatusParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct AskLatestMessageStatusParams: Codable {
    let data: [Message]
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    struct Message: Codable {
        let conversationId: String
        
        private enum CodingKeys: String, CodingKey {
            case conversationId = "conversation_id"
        }
    }
}
