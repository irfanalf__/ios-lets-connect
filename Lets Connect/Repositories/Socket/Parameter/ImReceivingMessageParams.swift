//
//  ImReceivingMessageParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct ImReceivingMessageParams: Codable {
    let conversationId: String
    let senderId: Int
    
    private enum CodingKeys: String, CodingKey {
        case conversationId = "conversation_id"
        case senderId = "sender_id"
    }
}
