//
//  SomeoneSendMessageResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SomeoneSendMessageResponse: BaseResponse, Codable {
    var status: Bool
    var message: String
    let conversationId: String
    let receiverId: String
    let conversation: String
    let replyTo: String?
    let receivedByServerAt: Double
    let sender: Sender
    
    private enum CodingKeys: String, CodingKey{
        case message
        case status
        case conversationId = "conversation_id"
        case receiverId = "receiver_id"
        case conversation
        case replyTo = "reply_to"
        case receivedByServerAt = "received_by_server_at"
        case sender
    }
    
    struct Sender: Codable {
        let id: Int
        let email: String
        let fullname: String
        let image: String
        let username: String
        
        private enum CodingKeys: String, CodingKey {
            case id
            case email
            case fullname
            case image
            case username
        }
    }
}
