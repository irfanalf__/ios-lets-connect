//
//  SomeoneOfflineResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SomeoneOfflineResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let lastOnline: Double
    let idUser: String
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case lastOnline = "last_online"
        case idUser = "id_user"
    }
}
