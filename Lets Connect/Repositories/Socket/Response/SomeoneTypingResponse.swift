//
//  SomeoneTypingResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SomeoneTypingResponse: BaseResponse, Codable {
    var status: Bool
    var message: String
    var fromId: String
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case fromId = "from_id"
    }
}
