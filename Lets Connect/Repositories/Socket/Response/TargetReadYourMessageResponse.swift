//
//  TargetReadYourMessageResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct TargetReadYourMessageResponse: BaseResponse, Codable {
    var status: Bool
    var message: String
    let receiverId: Int
    let readAt: Double
    
    private enum CodingKeys: String, CodingKey {
        case status
        case message
        case receiverId = "receiver_id"
        case readAt = "read_at"
    }
}
