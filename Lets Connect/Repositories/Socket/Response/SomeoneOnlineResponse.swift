//
//  SomeoneOnlineResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SomeoneOnlineResponse: BaseResponse, Codable {
    var status: Bool
    var message: String
    let idUser: String
    
    private enum CodingKeys: String, CodingKey {
        case status
        case message
        case idUser = "id_user"
    }
}
