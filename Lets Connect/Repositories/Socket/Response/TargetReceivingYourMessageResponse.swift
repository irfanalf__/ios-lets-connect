//
//  TargetReceivingYourMessageResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct TargetReceivingYourMessageResponse: BaseResponse, Codable {
    var status: Bool
    var message: String
    let conversationId: String
    let senderId: String
    let sentAt: UInt64
    
    private enum CodingKeys: String, CodingKey{
        case message
        case status
        case conversationId = "conversation_id"
        case senderId = "sender_id"
        case sentAt = "sent_at"
    }
}
