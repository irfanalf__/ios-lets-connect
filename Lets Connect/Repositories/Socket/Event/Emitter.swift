//
//  Emitter.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

enum Emitter: String {
    case GoToBackground = "i'm going to background"
    case GoToForeground = "i'm going to foreground"
    case ReceivingMessage = "i'm receiving message"
    case ReadMessage = "i'm reading message"
    case AskForDelayedMessage = "give me my delayed message"
    case AskForUpdatedMessageReport = "give me my latest report message"
    case Typing = "i'm typing"
    case SendMessage = "i'm sending message"
}
