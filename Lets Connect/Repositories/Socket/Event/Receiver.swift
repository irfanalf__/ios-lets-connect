//
//  Receiver.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

enum Receiver: String {
    case Connect = "connect"
    case SomeoneOnline = "someone online"
    case SomeoneOffline = "someone offline"
    case SomeoneTyping = "someone typing"
    case SomeoneSendMessage = "someone send you message"
    case TargetReceivingYourMessage = "target receiving your message"
    case TargetReceivingYourDelayedMessage = "target receiving your delayedmessage"
    case TargetReadYourMessage = "target read your message"
    case Disconnect = "disconnect"
    case Exception = "default exception"
}
