//
//  RemoteRepositories.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import Alamofire
import Combine


class RemoteRepository {
    static let shared = RemoteRepository()
    
    private static let baseUrl = ""
    
    private init(){}
    
    /// Registration API
    func getRegistrationData() -> AnyPublisher<RegistrationDataResponse, NetworkException>{
        return Future { promise in
            AF.request("registration/registration_data", method: .get)
                .responseDecodable(of: RegistrationDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func registerAsClient(param: SignUpClientParams) -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            let parameter = try! JSONEncoder().encode(param)
            
            var request = try! URLRequest(url: "https://".asURL().appendingPathComponent("registration/client"))
            request.method = .post
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = parameter
            
            AF.request(request).responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func registerAsVolunteer(param: SignUpVolunteerParams) -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            let parameter = try! JSONEncoder().encode(param)
            
            var request = try! URLRequest(url: "https://".asURL().appendingPathComponent("registration/volunteer"))
            request.method = .post
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = parameter
            
            AF.request(request).responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Authentication API
    func login(param: LoginParams) -> AnyPublisher<UserResponse, NetworkException>{
        return Future { promise in
            let parameter = try! JSONEncoder().encode(param)
            
            var request = try! URLRequest(url: "https://".asURL().appendingPathComponent("login"))
            request.method = .post
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = parameter
            
            AF.request(request).responseDecodable(of: UserResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func logout(id: Int) -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            AF.request("logout", method: .post, parameters: [ "id":id ]).responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Dashboard Discover API
    func discoverPerson(query: String) -> AnyPublisher<DiscoverPersonResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/all", method: .post, parameters: [ "query":query ])
                .responseDecodable(of: DiscoverPersonResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func discoverPersonByRole(query: String, role: String) -> AnyPublisher<DiscoverByRoleResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/role", method: .post, parameters: [ "query":query, "role":role ])
                .responseDecodable(of: DiscoverByRoleResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func discoverPersonBySubRole(query: String, role: String, subrole: String) -> AnyPublisher<DiscoverByRoleResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/subrole", method: .post, parameters: [ "query":query, "role":role, "subrole":subrole ])
                .responseDecodable(of: DiscoverByRoleResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Chat API
    func getLastSeen(idUser: String) -> AnyPublisher<LastSeenResponse, NetworkException>{
        return Future { promise in
            AF.request("chat/lastseen/\(idUser)", method: .post).responseDecodable(of: LastSeenResponse.self) { response in
                response.consumeResult(
                    onSuccess: { (data) in promise(.success(data)) },
                    onError: { (error) in promise(.failure(error)) }
                )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Utilities API
    func checkVersion(currentVersionCode: Int) -> AnyPublisher<CheckVersionResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/subrole", method: .post, parameters: [ "current_version_code":currentVersionCode ])
                .responseDecodable(of: CheckVersionResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
}
