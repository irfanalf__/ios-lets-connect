//
//  Exception.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import Alamofire

class NetworkException: Error {}

class SessionDeinitializedError: NetworkException {}
class SessionInvalidateError: NetworkException {}
class ExplicitlyCancelledError: NetworkException {}
class InvalidURLError: NetworkException {}
class ParameterEncodingError: NetworkException {}
class ParameterEncoderError: NetworkException {}
class MultipartEncodingError: NetworkException {}
class RequestAdaptationError: NetworkException {}
class ResponseValidationError: NetworkException {}
class ResponseSerializationError: NetworkException {}
class ServerTrustEvaluationError: NetworkException {}
class RequestRetryError: NetworkException {}
class CreateUploadableError: NetworkException {}
class CreateUrlRequestError: NetworkException {}
class DownloadedFileMoveError: NetworkException {}
class SessionTaskError: NetworkException {}
class RequestValidationError: NetworkException {}
class HttpError: NetworkException {}
class ResponseError: NetworkException {}
