//
//  LastSeenResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct LastSeenResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let lastOnline: Double
    let statusOnline: Int
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case lastOnline = "last_online"
        case statusOnline = "status_online"
    }
}
