//
//  ReviewResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct ReviewResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let data: [Review]
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case data
    }
    
    struct Review: Codable{
        let comment: String
        let date: String
        let idReview: String
        let rating: String
        
        private enum CodingKeys: String, CodingKey{
            case comment
            case date
            case idReview = "id_review"
            case rating
        }
        
    }
}
