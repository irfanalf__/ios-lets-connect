//
//  CheckVersionResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct CheckVersionResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let data: [Data]
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case data
    }
    
    struct Data: Codable {
        let versionCode: String
        let versionName: String
        
        private enum CodingKeys: String, CodingKey{
            case versionCode = "version_code"
            case versionName = "version_name"
        }
    }
}
