//
//  UserResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct UserResponse: BaseResponse, Codable{
    var message: String
    var status: Bool
    let user: User
    let specializations: [Specialization]
    
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case user
        case specializations
    }
    
    struct Specialization: Codable {
        let id: String
        let idSpecialize: String
        let specializeName: String?
        
        private enum CodingKeys: String, CodingKey {
            case id
            case idSpecialize = "id_specialize"
            case specializeName = "specialize_name"
        }
    }
    
    struct User: Codable {
        let aboutMe: String?
        let birthday: String?
        let certificate: String?
        let communicationId: String?
        let email: String?
        let fullname: String?
        let gender: String?
        let id: Int
        let image: String?
        let licensing: String?
        let linkvideo: String?
        let locationCode: String
        let locationName: String
        let ourSessions: String?
        let trialPer: String?
        let ratePer: String?
        let roleId: String
        let roleName: String?
        let speakFluent: String?
        let typeNamerole: String
        let username: String?
        let workWith: String?
        let yearsExp: String?
        
        private enum CodingKeys: String, CodingKey {
            case aboutMe = "about_me"
            case birthday
            case certificate
            case communicationId = "communication_id"
            case email
            case fullname
            case gender
            case id
            case image
            case licensing
            case linkvideo
            case locationCode = "location_code"
            case locationName = "location_name"
            case ourSessions = "our_sessions"
            case trialPer = "trial_per"
            case ratePer = "rate_per"
            case roleId = "role_id"
            case roleName = "role_name"
            case speakFluent = "speak_fluent"
            case typeNamerole = "type_namerole"
            case username
            case workWith = "work_with"
            case yearsExp = "years_exp"
        }
    }
    
}
