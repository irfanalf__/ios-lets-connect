//
//  NoDataResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 06/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct NoDataResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
        
    private enum CodingKeys: String, CodingKey {
        case message
        case status
    }
}
