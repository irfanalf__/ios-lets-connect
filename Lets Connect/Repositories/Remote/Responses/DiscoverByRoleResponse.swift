//
//  DiscoverByRoleResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct DiscoverByRoleResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let data: DiscoverPersonResponse.Data.Person
    
    private enum CodingKeys: String, CodingKey{
        case message
        case status
        case data
    }
}
