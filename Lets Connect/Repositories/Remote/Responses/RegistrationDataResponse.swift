//
//  RegistrationDataResponse.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct RegistrationDataResponse: BaseResponse, Codable {
    var message: String
    var status: Bool
    let location: [Location]?
    let specialize: [Specialize]?

    private enum CodingKeys: String, CodingKey{
        case message
        case status
        case location
        case specialize
    }
    
    struct Location: Codable {
        let locationCode: String
        let locationName: String

        private enum CodingKeys: String, CodingKey {
            case locationCode = "location_code"
            case locationName = "location_name"
        }
    }
    
    struct Specialize: Codable {
        let id: String
        let specializeName: String
        
        private enum CodingKeys: String, CodingKey{
            case id
            case specializeName = "specialize_name"
        }
    }
}
