//
//  RemoteRepositories.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import Alamofire
import Combine


class MockRepository {
    static let shared = MockRepository(baseUrl: "https://virtserver.swaggerhub.com/irfanalfiansyah10/lets-connect-mock-api/1.0.0")
    
    private var baseUrl = ""
    
    private init(baseUrl: String){
        self.baseUrl = baseUrl
    }
    
    /// Registration API
    func getRegistrationData() -> AnyPublisher<RegistrationDataResponse, NetworkException>{
        return Future { promise in
            AF.request("registration/registration_data", method: .get)
                .responseDecodable(of: RegistrationDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func registerAsClient() -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            AF.request("registration/client", method: .get)
                .responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func registerAsVolunteer() -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            AF.request("registration/volunteer", method: .get)
                .responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Authentication API
    func login() -> AnyPublisher<UserResponse, NetworkException>{
        return Future { promise in
            AF.request("login", method: .get)
                .responseDecodable(of: UserResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func logout() -> AnyPublisher<NoDataResponse, NetworkException>{
        return Future { promise in
            AF.request("logout", method: .get)
                .responseDecodable(of: NoDataResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Dashboard Discover API
    func discoverPerson() -> AnyPublisher<DiscoverPersonResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/all", method: .get)
                .responseDecodable(of: DiscoverPersonResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func discoverPersonByRole() -> AnyPublisher<DiscoverByRoleResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/role", method: .get)
                .responseDecodable(of: DiscoverByRoleResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    func discoverPersonBySubRole() -> AnyPublisher<DiscoverByRoleResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/subrole", method: .get)
                .responseDecodable(of: DiscoverByRoleResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Chat API
    func getLastSeen() -> AnyPublisher<LastSeenResponse, NetworkException>{
        return Future { promise in
            AF.request("chat/lastseen)", method: .get).responseDecodable(of: LastSeenResponse.self) { response in
                response.consumeResult(
                    onSuccess: { (data) in promise(.success(data)) },
                    onError: { (error) in promise(.failure(error)) }
                )
            }
        }.eraseToAnyPublisher()
    }
    
    ///Utilities API
    func checkVersion() -> AnyPublisher<CheckVersionResponse, NetworkException>{
        return Future { promise in
            AF.request("discover/subrole", method: .get)
                .responseDecodable(of: CheckVersionResponse.self) { response in
                    response.consumeResult(
                        onSuccess: { (data) in promise(.success(data)) },
                        onError: { (error) in promise(.failure(error)) }
                    )
            }
        }.eraseToAnyPublisher()
    }
}
