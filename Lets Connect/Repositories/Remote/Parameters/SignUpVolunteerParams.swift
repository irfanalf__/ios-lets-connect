//
//  SignUpVolunteerParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SignUpVolunteerParams: Codable {
    let organization: String?
    let fullname: String?
    let gender: String?
    let locationCode: String?
    let email: String?
    let password: String?
    let aboutMe: String?
    
    private enum CodingKeys: String, CodingKey {
        case organization
        case fullname
        case gender
        case locationCode = "location_cde"
        case email
        case password
        case aboutMe = "about_me"
    }
}
