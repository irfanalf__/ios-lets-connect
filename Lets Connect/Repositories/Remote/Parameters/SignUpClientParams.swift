//
//  SignUpClientParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct SignUpClientParams: Codable {
    let username: String?
    let email: String?
    let password: String?
    
    private enum CodingKeys: String, CodingKey {
        case username
        case email
        case password
    }
}
