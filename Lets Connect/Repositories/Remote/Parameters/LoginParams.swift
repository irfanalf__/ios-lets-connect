//
//  LoginParams.swift
//  Lets Connect
//
//  Created by Lets Connect on 13/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

struct LoginParams: Codable {
    let email: String?
    let password: String?
    let firebaseToken: String?
    
    private enum CodingKeys: String, CodingKey {
        case email
        case password
        case firebaseToken = "firebase_token"
    }
}
