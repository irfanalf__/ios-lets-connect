//
//  Login.swift
//  Lets Connect
//
//  Created by Lets Connect on 04/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct Login: View {
    @ObservedObject var vm = LoginVM()
    
    var body: some View {
        LoadingView(isShowing: self.$vm.showActivityIndicator){
            VStack(alignment: HorizontalAlignment.leading){
                Group {
                    Text(StringResources.login_email)
                        .font(Font.robotoRegular(size: 16))
                    TextField(StringResources.login_hint_email, text: self.$vm.email)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    if(!self.vm.validateEmail){
                        Text(self.vm.errorEmail)
                            .font(Font.robotoRegular(size: 14))
                            .foregroundColor(Color.red5757)
                    }
                }
                Spacer().frame(height: 20)
                Group {
                    Text(StringResources.login_password)
                        .font(Font.robotoRegular(size: 16))
                    TextField(StringResources.login_hint_password, text: self.$vm.password)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    if(!self.vm.validatePassword){
                        Text(self.vm.errorPassword)
                            .font(Font.robotoRegular(size: 14))
                            .foregroundColor(Color.red5757)
                    }
                }
                Spacer().frame(height: 20)
                HStack{
                    Text(StringResources.login_forgot_password)
                        .font(.robotoRegular(size: 14))
                    Spacer()
                    Button(StringResources.login_login){
                        self.vm.login()
                    }
                    .padding(.horizontal, 20)
                    .padding(.vertical, 10)
                    .background(Color.black)
                    .foregroundColor(Color.white)
                    .cornerRadius(10)
                    .font(.robotoRegular(size: 20))
                }
                Spacer()
                Button(action: { self.vm.showActionSignUp = true }){
                    HStack{
                        Spacer()
                        Text(StringResources.login_not_member_yet)
                            .font(.robotoRegular(size: 18))
                        Text(StringResources.login_signup)
                            .font(.robotoBold(size: 18))
                        Spacer()
                    }
                }.actionSheet(isPresented: self.$vm.showActionSignUp) {
                    ActionSheet(
                        title: Text(StringResources.login_sign_up_options),
                        message: Text(StringResources.login_sign_up_choices),
                        buttons: [
                            .default(Text(StringResources.login_client)),
                            .default(Text(StringResources.login_professional)),
                            .default(Text(StringResources.login_supporter)),
                            .default(Text(StringResources.login_volunteer)),
                            .destructive(Text(StringResources.login_cancel))
                        ]
                    )
                }
                .foregroundColor(Color.black)
                .padding()
            }
            .padding(.vertical)
            .padding(.horizontal, 50)
            .navigationBarTitle(StringResources.login_login, displayMode: .automatic)
            .navigationBarBackButtonHidden(self.vm.showActivityIndicator)
        }
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Group {
           Login()
              .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
              .previewDisplayName("iPhone 8 Plus")
        }
    }
}
