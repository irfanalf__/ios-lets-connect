//
//  LoginVM.swift
//  Lets Connect
//
//  Created by Lets Connect on 04/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

class LoginVM: ObservableObject {
    private let mockRepository = MockRepository.shared
    private let remoteRepository = RemoteRepository.shared
    
    @Published var email = "" {
        didSet {
            if(email.isEmpty){
                validateEmail = false
                errorEmail = StringResources.error_email_empty
            }else {
                validateEmail = true
            }
        }
    }
    @Published var password = "" {
        didSet {
            if(password.isEmpty){
                validatePassword = false
                errorPassword = StringResources.error_password_empty
            }else {
                validatePassword = true
            }
        }
    }
    
    @Published var validateEmail = false {
        didSet {
            validateAll = validateEmail && validatePassword
        }
    }
    @Published var errorEmail: LocalizedStringKey = StringResources.error_email_empty
    @Published var validatePassword = false {
        didSet {
            validateAll = validateEmail && validatePassword
        }
    }
    @Published var errorPassword: LocalizedStringKey = StringResources.error_password_empty
    @Published var validateAll = false
    
    @Published var showActionSignUp = false
    @Published var showActivityIndicator = false
    
    public func login(){
        /**self.showActivityIndicator = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.showActivityIndicator = false
        })*/
    }
}

