//
//  Main.swift
//  Lets Connect
//
//  Created by Lets Connect on 14/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct Main: View {
    var body: some View {
        NavigationView{
            Onboarding()
        }
    }
}
