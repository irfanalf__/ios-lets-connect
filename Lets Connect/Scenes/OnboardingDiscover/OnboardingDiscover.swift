//
//  OnboardingDiscover.swift
//  Lets Connect
//
//  Created by Lets Connect on 05/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

//
//  Onboarding.swift
//  Lets Connect
//
//  Created by Lets Connect on 09/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct OnboardingDiscoverNavigation: View {
    @Binding var isActive: Bool
    
    var body: some View {
        NavigationLink(destination: OnboardingDiscover(), isActive: self.$isActive){ EmptyView() }
    }
}

struct OnboardingDiscover: View {
    var body: some View {
        ScrollView(.vertical) {
            VStack{
                Spacer().frame(height: 10)
                DiscoverOnboardingPerRoleCell()
                DiscoverOnboardingPerRoleCell()
            }
        }
        .navigationBarTitle(StringResources.onboarding_discover, displayMode: .inline)
        .navigationBarItems(trailing:
            NavigationLink(destination: Login()){
                Text(StringResources.onboarding_login)
                .font(Font.robotoBold(size: 18))
                .padding(.vertical, 6)
                .padding(.horizontal, 10)
                .foregroundColor(Color.white)
                .background(Color.red5757)
                .cornerRadius(10)
            }
        )
    }
}

struct DiscoverOnboardingPerRoleCell: View {
    var body: some View {
        VStack(alignment: HorizontalAlignment.leading) {
            HStack(alignment: .top) {
                Text("Psychologists, Counselors and Therapists for you")
                Spacer()
                Image(ImageResources.ic_arrow_next).padding(.top, 6)
            }.padding([.horizontal])

            Spacer().frame(height: 10)
            
            ScrollView(.horizontal){
                HStack{
                    Spacer().frame(width: 10)
                    DiscoverOnboardingPersonCell()
                    Spacer().frame(width: 10)
                    DiscoverOnboardingPersonCell()
                    Spacer().frame(width: 10)
                }
            }
            
        }
    }
}

struct DiscoverOnboardingPersonCell: View {
    var body: some View {
        VStack(alignment: .leading) {
            Spacer().frame(height: 10)
            HStack{
                Text("Scarlett Johansson")
                    .font(Font.robotoBold(size: 14))
                Spacer()
                Image("ic_flag_ad")
                    .resizable()
                    .frame(width: 22, height: 16)
            }
            HStack{
                Text("Counselor")
                    .font(Font.robotoRegular(size: 12))
                    .foregroundColor(.red5757)
                Spacer()
                // TODO: add rating bar
            }
            VStack{
                Image(ImageResources.vector_logo)
                .resizable()
                .frame(width: 150, height: 150)
            }.frame(minWidth: 0, maxWidth: .infinity)
            VStack{
                Text("I specialize in: \n-aaa\n-aaaa\n-aaaa")
                    .font(Font.robotoRegular(size: 12))
            }
            Spacer().frame(height: 10)
            Text("Lorem ipsum dolor sit amet consectetur, adipisicing elit. Earum neque eum expedita debitis architecto ea nesciunt? Eos ... Read more Lorem ipsum dolor sit amet consectetur, adipisicing elit. Earum neque eum expedita debitis architecto ea nesciunt? Eos ... Read more")
                .font(Font.robotoRegular(size: 12))
                .lineLimit(3)
            Spacer()
            Button(StringResources.onboarding_connect_with(name: "Scarlett")) {
                    
            }.foregroundColor(Color.white)
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .background(Color.black)
            .cornerRadius(10)
            Spacer().frame(height: 5)
        }
        .frame(width: 280, height: 410)
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 15)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.4), lineWidth: 1)
        )
        .padding(.bottom)
    }
}

struct OnboardingDiscover_Previews: PreviewProvider {
    static var previews: some View {
        Group {
           Onboarding()
              .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
              .previewDisplayName("iPhone 8 Plus")
        }
    }
}
