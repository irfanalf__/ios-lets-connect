//
//  Onboarding.swift
//  Lets Connect
//
//  Created by Lets Connect on 09/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct OnboardingNavigation: View {
    @Binding var isActive: Bool
    
    var body: some View {
        NavigationLink(destination: Onboarding(), isActive: self.$isActive){ EmptyView() }
    }
}

struct Onboarding: View {
    @ObservedObject var vm: OnboardingVM = OnboardingVM()
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView{
                VStack{
                    //Navigation group
                    Group {
                        OnboardingDiscoverNavigation(isActive: self.$vm.isNavigationOnboardingDiscover)
                    }
                    
                    Image(ImageResources.vector_logo)
                    Spacer()
                        .frame(height: 30)
                    Text(StringResources.onboarding_description)
                        .multilineTextAlignment(.center)
                        .font(Font.robotoRegular(size: 18))
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                    Spacer()
                        .frame(height: 40)
                    Text(StringResources.onboarding_visit_us)
                        .font(Font.robotoRegular(size: 16))
                    Button(action: {
                        if let url = URL(string: "https://www.letsconnectproject.com") {
                            UIApplication.shared.open(url)
                        }
                    }){
                        Text(StringResources.onboarding_web)
                        .font(Font.robotoMedium(size: 22))
                        .foregroundColor(.red5757)
                    }
                    Spacer()
                        .frame(height: 40)
                    Button(action: { self.vm.gotoOnboardingDiscover() }){
                        Text(StringResources.onboarding_tap)
                        .font(Font.robotoMedium(size: 22))
                        .foregroundColor(.blueBCFF)
                    }
                }
                .frame(width: geometry.size.width)
                .frame(minHeight: geometry.size.height)
            }
        }
        .navigationBarHidden(true)
        .navigationBarTitle("")
    }
}

struct Onboarding_Previews: PreviewProvider {
    static var previews: some View {
        Group {
           Onboarding()
              .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
              .previewDisplayName("iPhone 8 Plus")
        }
    }
}
