//
//  OnboardingVM.swift
//  Lets Connect
//
//  Created by Lets Connect on 14/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import Combine

class OnboardingVM: ObservableObject {
    @Published var isNavigationOnboardingDiscover: Bool = false
    
    func gotoOnboardingDiscover(){
        self.isNavigationOnboardingDiscover = true
    }
}
