//
//  SignUpVolunteer.swift
//  Lets Connect
//
//  Created by Lets Connect on 02/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct SignUpVolunteerNavigation: View {
    @Binding var isActive: Bool
    
    var body: some View {
        NavigationLink(destination: SignUpVolunteerStep1(), isActive: self.$isActive){ EmptyView() }
    }
}

struct SignUpVolunteerStep1: View {
    @State var organization: String = ""
    @State var fullName: String = ""
    @State var screenName: String = ""
    @State var photo: String = ""
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: HorizontalAlignment.leading){
                    VStack{
                        Text("Let's Connect Project")
                            .font(.robotoBold(size: 22))
                        Image(ImageResources.vector_logo)
                            .resizable()
                            .frame(width: 100, height: 100)
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Organization").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $organization).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Screen Name").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $screenName).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Full Name").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $fullName).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Photo").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $photo).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    VStack{
                        Spacer().frame(height: 20)
                        Button("Next"){
                            
                        }
                        .padding()
                        .background(Color.black)
                        .foregroundColor(Color.white)
                        .cornerRadius(10)
                        .font(.robotoRegular(size: 20))
                        Spacer().frame(height: 20)
                        HStack{
                            Spacer()
                            Text("Already a member? ")
                                .font(.robotoRegular(size: 18))
                            Text("Log In")
                                .font(.robotoBold(size: 18))
                            Spacer()
                        }
                        Spacer().frame(height: 20)
                    }.frame(minWidth: 0, maxWidth: .infinity).padding(.horizontal, 1)
                }
                .padding(.horizontal, 50)
                .padding(.vertical)
            }.navigationBarTitle("Sign up as VOLUNTEER")
        }
    }
}

struct SignUpVolunteerStep2: View {
    @State var gender: String = ""
    @State var location: String = ""
    @State var aboutMe: String = ""
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: HorizontalAlignment.leading){
                    VStack{
                        Text("Let's Connect Project")
                            .font(.robotoBold(size: 22))
                        Image(ImageResources.vector_logo)
                            .resizable()
                            .frame(width: 100, height: 100)
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Gender").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $gender).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Location").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $location).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("About Me").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $aboutMe).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    
                    VStack{
                        Spacer().frame(height: 20)
                        Button("Next"){
                            
                        }
                        .padding()
                        .background(Color.black)
                        .foregroundColor(Color.white)
                        .cornerRadius(10)
                        .font(.robotoRegular(size: 20))
                        Spacer().frame(height: 20)
                    }.frame(minWidth: 0, maxWidth: .infinity).padding(.horizontal, 1)
                }
                .padding(.horizontal, 50)
                .padding(.vertical)
            }.navigationBarTitle("Sign up as VOLUNTEER")
        }
    }
}

struct SignUpVolunteerStep3: View {
    @State var email: String = ""
    @State var password: String = ""
    @State var repeatPassword: String = ""
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: HorizontalAlignment.leading){
                    VStack{
                        Text("Let's Connect Project")
                            .font(.robotoBold(size: 22))
                        Image(ImageResources.vector_logo)
                            .resizable()
                            .frame(width: 100, height: 100)
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Email").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $email).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Password").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $password).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Repeat Password").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $repeatPassword).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 20)
                        CheckBox<SimpleCheckBoxItem>(
                            item: SimpleCheckBoxItem(message: "I have read and agreed with the Terms and Conditions")
                        )
                        Spacer().frame(height: 10)
                        CheckBox<SimpleCheckBoxItem>(
                            item: SimpleCheckBoxItem(message: "I'm over 13 years of age")
                        )
                    }.padding(.horizontal, 1)
                    VStack{
                        Spacer().frame(height: 20)
                        Button("Sign Up"){
                            
                        }
                        .padding()
                        .background(Color.black)
                        .foregroundColor(Color.white)
                        .cornerRadius(10)
                        .font(.robotoRegular(size: 20))
                        Spacer().frame(height: 20)
                    }.frame(minWidth: 0, maxWidth: .infinity).padding(.horizontal, 1)
                }
                .padding(.horizontal, 50)
                .padding(.vertical)
            }.navigationBarTitle("Sign up as VOLUNTEER")
        }
    }
}


struct SignUpVolunteer_Preview: PreviewProvider {
    static var previews: some View {
        Group {
            SignUpVolunteerStep1()
               .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
               .previewDisplayName("iPhone 8 Plus")
            SignUpVolunteerStep2()
               .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
               .previewDisplayName("iPhone 8 Plus")
            SignUpVolunteerStep3()
              .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
              .previewDisplayName("iPhone 8 Plus")
        }
    }
}
