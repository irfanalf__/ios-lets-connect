//
//  SignUpClient.swift
//  Lets Connect
//
//  Created by Lets Connect on 02/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct SignUpClientNavigation: View {
    @Binding var isActive: Bool
    
    var body: some View {
        NavigationLink(destination: SignUpClient(), isActive: self.$isActive){ EmptyView() }
    }
}

struct SignUpClient: View {
    @State var screenName: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var repeatPassword: String = ""
    
    var body: some View {
        NavigationView{
            ScrollView {
                VStack(alignment: HorizontalAlignment.leading){
                    VStack{
                        Text("Let's Connect Project")
                            .font(.robotoBold(size: 22))
                        Image(ImageResources.vector_logo)
                            .resizable()
                            .frame(width: 100, height: 100)
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Screen Name").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $screenName).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Username").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $email).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Username").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $email).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 10)
                        Text("Username").font(.robotoRegular(size: 16))
                        TextField("example@email.com", text: $email).padding(10).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 1))
                    }.padding(.horizontal, 1)
                    Group{
                        Spacer().frame(height: 20)
                        CheckBox<SimpleCheckBoxItem>(
                            item: SimpleCheckBoxItem(message: "I have read and agreed with the Terms and Conditions")
                        )
                        Spacer().frame(height: 10)
                        CheckBox<SimpleCheckBoxItem>(
                            item: SimpleCheckBoxItem(message: "I'm over 13 years of age")
                        )
                    }.padding(.horizontal, 1)
                    VStack{
                        Spacer().frame(height: 20)
                        Button("Sign Up"){
                            
                        }
                        .padding()
                        .background(Color.black)
                        .foregroundColor(Color.white)
                        .cornerRadius(10)
                        .font(.robotoRegular(size: 20))
                        Spacer().frame(height: 20)
                        HStack{
                            Spacer()
                            Text("Already a member? ")
                                .font(.robotoRegular(size: 18))
                            Text("Log In")
                                .font(.robotoBold(size: 18))
                            Spacer()
                        }
                        Spacer().frame(height: 20)
                    }.frame(minWidth: 0, maxWidth: .infinity).padding(.horizontal, 1)
                }
                .navigationBarTitle("Sign up as CLIENT")
                .padding(.horizontal, 50)
                .padding(.vertical)
            }
        }
    }
}

struct SignUpClient_Preview: PreviewProvider {
    static var previews: some View {
        Group {
           SignUpClient()
              .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
              .previewDisplayName("iPhone 8 Plus")
        }
    }
}
