//
//  BaseRequest.swift
//  Lets Connect
//
//  Created by Lets Connect on 10/08/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation

protocol BaseResponse{
    var message: String { get }
    var status: Bool { get }
}
