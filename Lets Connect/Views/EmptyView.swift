//
//  EmptyView.swift
//  Lets Connect
//
//  Created by Lets Connect on 14/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

struct EmptyView: View {
    var body: some View {
        Spacer().frame(width: 0, height: 0)
    }
}
