//
//  CheckBox.swift
//  Lets Connect
//
//  Created by Lets Connect on 02/09/20.
//  Copyright © 2020 Lets Connect. All rights reserved.
//

import Foundation
import SwiftUI

protocol CheckBoxItem {
    var message: String { get }
}

struct SimpleCheckBoxItem: CheckBoxItem {
    var message: String
}

struct CheckBox<T: CheckBoxItem>: View {
    let item: T
    let onCheckedChange: (T, Bool) -> () = { _, _ in }
    let boxSize: CGFloat = 12
    let textSize: CGFloat = 12
    
    @State var isChecked: Bool = false
    
    var body: some View {
        Button(action:{
            self.isChecked.toggle()
            self.onCheckedChange(self.item, self.isChecked)
        }) {
            HStack(alignment: VerticalAlignment.center, spacing: 10) {
                Image(systemName: self.isChecked ? "checkmark.square" : "square")
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: self.boxSize, height: self.boxSize)
                Text(item.message)
                    .font(.robotoRegular(size: self.textSize))
                Spacer()
            }.foregroundColor(Color.black)
        }
        .foregroundColor(Color.white)
    }
}

struct CheckBox_Preview: PreviewProvider {
    static var previews: some View {
        Group {
           CheckBox<SimpleCheckBoxItem>(item: SimpleCheckBoxItem(message: "Checkbox"))
            .previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
            .previewDisplayName("iPhone 8 Plus")
        }
    }
}
